!function($){
	$('.tab-header-item').on('click', function(e){
		var $this      = $(this),
			$tab 	   = $this.parents('.easy-tab'),
			$tabTitles = $tab.find('.tab-header-item'),
			$tabBodies = $tab.find('.tab-body-item'),
			curIndex   = 0;
		
		curIndex = $tabTitles.index($this);

		e.preventDefault();

		$tabTitles.removeClass('active');
		$tabBodies.removeClass('active');

		$this.addClass('active');
		$tabBodies.eq(curIndex).addClass('active');
	});
}(jQuery);